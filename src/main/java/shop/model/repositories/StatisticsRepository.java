package shop.model.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import shop.model.entities.Statistics;

public interface StatisticsRepository extends PagingAndSortingRepository<Statistics, Integer> {
    
}
