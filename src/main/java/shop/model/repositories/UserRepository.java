package shop.model.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import shop.model.entities.User;

public interface UserRepository extends JpaRepository<User, Integer> {

    User findByUsername(String username);
}
