package shop.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import shop.controllers.dto.CategoryDto;
import shop.controllers.dto.ProductDto;
import shop.model.entities.Category;
import shop.model.entities.Product;
import shop.model.repositories.CategoriesRepository;
import shop.model.repositories.ProductsRepository;

@Controller
public class AdminController {

    private final CategoriesRepository categoriesRepository;
    private final ProductsRepository productsRepository;

    @Autowired
    public AdminController(CategoriesRepository categoriesRepository,
                           ProductsRepository productsRepository) {
        this.categoriesRepository = categoriesRepository;
        this.productsRepository = productsRepository;
    }

    @GetMapping("/admin/category")
    public String categoryManagement(Model model) {
        final CategoryDto category = new CategoryDto();

        model.addAttribute("category", category);

        return "adminCategory";
    }

    @PostMapping("/admin/category")
    public String addCategory(@ModelAttribute("category") CategoryDto categoryDto) {
        final String parentName = categoryDto.getParentName();

        Category parentCategory = null;

        if (parentName != null && !parentName.isEmpty()) {
            parentCategory = categoriesRepository.findByName(parentName);
        }

        final Category newCategory = new Category(categoryDto.getName(), parentCategory);
        categoriesRepository.save(newCategory);

        return "redirect:/admin/category";
    }

    @GetMapping("/admin/product")
    public String productManagement(Model model) {
        final ProductDto product = new ProductDto();

        model.addAttribute("product", product);

        return "adminProduct";
    }

    @PostMapping("/admin/product")
    public String addProduct(@ModelAttribute("product") ProductDto productDto) {
        final Category category = categoriesRepository.findByName(productDto.getCategoryName());

        final Product newProduct = new Product(
                productDto.getName(),
                productDto.getDescription(),
                productDto.getPrice(),
                productDto.getImageUrl(),
                category);

        productsRepository.save(newProduct);

        return "redirect:/admin/product";
    }
}
