package shop.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import shop.model.entities.Statistics;
import shop.model.repositories.ProductsRepository;
import shop.model.repositories.StatisticsRepository;

import java.util.*;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summingInt;

@Controller
public class StatisticsController {
    private final static ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private StatisticsRepository statisticsRepository;
    private ProductsRepository productsRepository;

    @Autowired
    public StatisticsController(StatisticsRepository statisticsRepository,
                                ProductsRepository productsRepository) {
        this.statisticsRepository = statisticsRepository;
        this.productsRepository = productsRepository;
    }

    @RequestMapping("/statistics")
    public String statistics(Model model) {
        final Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, -5);
        final Date dateMinus5 = cal.getTime();

        final Iterable<Statistics> allStats = statisticsRepository.findAll();
        final Stream<Statistics> statisticsStream = StreamSupport.stream(allStats.spliterator(), false);

        final Map<String, Integer> countPerProduct = statisticsStream
                .filter(statistics -> statistics.getDate().after(dateMinus5))
                .collect(
                        groupingBy(
                                statistics -> statistics.getProduct().getName(),
                                summingInt(Statistics::getCount)));

        model.addAttribute("countPerProduct", countPerProduct);

        return "statistics";
    }

    @RequestMapping(value = "/update-statistics")
    public String updateStatistics(@RequestBody String data) throws JsonProcessingException {
        final Map<String, Object> dataMap = OBJECT_MAPPER.readValue(data, HashMap.class);

        final List<Integer> productIds = (List<Integer>) dataMap.get("productIds");
        final Map<String, Integer> counts = (Map<String, Integer>) dataMap.get("counts");

        for (int productId : productIds) {
            productsRepository.findById(productId)
                    .ifPresent(product -> {
                        final Statistics statistics = new Statistics(
                                product,
                                new Date(),
                                counts.get(String.valueOf(productId)));

                        statisticsRepository.save(statistics);
                    });
        }

        return "redirect:/catalog/1";
    }
}
